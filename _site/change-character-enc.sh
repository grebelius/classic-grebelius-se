#!/bin/bash

#for file in *.html; do
#	curr_enc=`file -b --mime-encoding $file`
#	#echo $curr_enc
#    iconv -f $curr_enc -t UTF-8 ${file} | dos2unix > ${file}.utf8
#done

#for file in *.htm; do
#	curr_enc=`file -b --mime-encoding $file`
#	#echo $curr_enc
#    iconv -f $curr_enc -t UTF-8 ${file} | dos2unix > ${file}.utf8
#done


#!/bin/bash
if [ $# -lt 1 ]
then
  echo "Use: "$0" <file_name>"
  echo "Convert files from ISO-8859-1 to UTF8"
  exit
fi

for i in $*
do
  if [ ! -f $i ]; then # Only convert text files
    continue
  fi
  # Generate temp file to avoid Bus error
  curr_enc=`file -b --mime-encoding $i`
  iconv -f $curr_enc -t utf-8 $i -o $i.tmp
  mv $i.tmp $i
done